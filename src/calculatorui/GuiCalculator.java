
package calculatorui;



/* This class is use to Manage Graphic Interface */


import java.awt.Color;
import java.awt.Font;
import static java.awt.Font.SERIF;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class GuiCalculator {

public GuiCalculator()
{
   
    arr=new ArrayList();
   
}
  public void guicalc()
  {
 DeclareButton();
 DeclareTextField();
 ButtonsetLocation();
 TextFieldsetLocation();
 ButtonsetText();
 setJFrame();  
 AddComponent();  

 getJFrame();
 Event(); 
  }
    /* This function Add Button  */
 public void DeclareButton()
 {
    
  
	 buttonstart=new JButton("clear");
      button1=new JButton("1");
      button1.setBackground(Color.WHITE);
      button2=new JButton("2");
       button3=new JButton("3");
       button4=new JButton("4"); 
       button5=new JButton("5");
   buttonstart.setBorder(null);    
      button6=new JButton("6");
     button7=new JButton("7"); 
      button8=new JButton("8"); 
        button9=new JButton("9");
         button0=new JButton("0");  
      buttonplus=new JButton("+");
      buttonminus=new JButton("-");
      buttondiv=new JButton("/");
      buttonmul=new JButton("*");
        buttonopen=new JButton("(");
      buttonclose=new JButton(")");
  buttonans=new JButton("Ans=");
       buttoncut=new JButton("Backremove");
     
 }
 public void DeclareTextField()
 {
     txtresult1=new JTextField(23); 
      Font font = new Font(SERIF, Font.BOLD, 20);
	txtresult1.setFont(font);
 }
 /* This function use to add component in the Frame */
 
 public void AddComponent()
 {
      frame.add(buttoncut);
  frame.add(txtresult1);
  frame.add(buttonstart);
  frame.add(button1);
  frame.add(button2);
  frame.add(button3);
  frame.add(button4);
  
  frame.add(button5);
  frame.add(button6);
  frame.add(button7);
  frame.add(button8);
  
  frame.add(button9);
  frame.add(button0);
  frame.add(buttonminus);
  frame.add(buttonplus);
  frame.add(buttondiv);
  frame.add(buttonmul);
  frame.add(buttonans);
  frame.add(buttonopen);
  frame.add(buttonclose);
 }
 /* This function use to set which Location to use button in the Frame */
 public void ButtonsetLocation()
 {
 /* This function use to Register event */
  button1.setBounds(8, 48, 60, 40);
  button2.setBounds(72, 48, 60, 40);
  button3.setBounds(136, 48, 60, 40);
  buttonplus.setBounds(200, 48, 60, 40);
  
    button4.setBounds(8, 95, 60, 40);
  button5.setBounds(72, 95, 60, 40);
  button6.setBounds(136, 95, 60, 40);
  buttonminus.setBounds(200, 95, 60, 40);
 
  button7.setBounds(8, 142, 60, 40);
  button8.setBounds(72, 142, 60, 40);
  button9.setBounds(136, 142, 60, 40);
  buttondiv.setBounds(200, 142, 60, 40);
  
  button0.setBounds(8, 189, 80, 40);
  buttonstart.setBounds(93, 189, 80, 40); 
  buttonmul.setBounds(178, 189, 80, 40);
  buttonans.setBounds(20, 236, 120, 40);
  buttoncut.setBounds(150, 236, 110, 40);
 buttonopen.setBounds(20, 284, 120, 40);
  buttonclose.setBounds(150, 284, 110, 40);
 
 }
 /* This function use to TextFild of the calculator */
 public void TextFieldsetLocation()
 {
  txtresult1.setBounds(2, 2, 273, 40);    
 }
 public void setJFrame()
 {
       frame=new JFrame("Calculator");
  frame.getContentPane().setBackground(Color.DARK_GRAY);
  //Container con=frame.getContentPane();
  //con.setLayout(new FlowLayout());
  frame.setLayout(null);
     
 }
 
 /* This function use display calculator in the screen */
 public void getJFrame()
 {
     frame.setSize(300, 380);
  
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
  
 frame.setVisible(true);
 }
 /* This function use to set Button Text Size  */
 public void ButtonsetText()
 {
 Font newButton=new Font(buttonplus.getFont().getName(),buttonplus.getFont().getStyle(),40);
   buttonplus.setFont(newButton);   
      Font newButton1=new Font(buttonminus.getFont().getName(),buttonminus.getFont().getStyle(),40);
   buttonminus.setFont(newButton1);   
      Font newButtonFont=new Font(buttonmul.getFont().getName(),buttonmul.getFont().getStyle(),40);
   buttonmul.setFont(newButtonFont);   
      Font newButton2=new Font(buttondiv.getFont().getName(),buttondiv.getFont().getStyle(),40);
   buttondiv.setFont(newButton2);   
    Font newButton3=new Font(buttonans.getFont().getName(),buttonans.getFont().getStyle(),20);
   buttonans.setFont(newButton3);   
      Font newButton4=new Font(buttoncut.getFont().getName(),buttoncut.getFont().getStyle(),12);
   buttoncut.setFont(newButton4);
      Font newButton5=new Font(buttonstart.getFont().getName(),buttonstart.getFont().getStyle(),20);
   buttonstart.setFont(newButton5);
        
 }
/* This function use to Register event */ 
 public void Event()
 {

       buttoncut.addActionListener(new EventHandler(this));
  
  buttonstart.addActionListener(new EventHandler(this));
  button1.addActionListener(new EventHandler(this));
 button2.addActionListener(new EventHandler(this));
 button3.addActionListener(new EventHandler(this));
 button4.addActionListener(new EventHandler(this));
 button5.addActionListener(new EventHandler(this));
 button6.addActionListener(new EventHandler(this));
 button7.addActionListener(new EventHandler(this));
 button8.addActionListener(new EventHandler(this));
 button9.addActionListener(new EventHandler(this));
 button0.addActionListener(new EventHandler(this));
 buttonans.addActionListener(new EventHandler(this));
 buttonplus.addActionListener(new EventHandler(this));
 buttonminus.addActionListener(new EventHandler(this));
 buttonmul.addActionListener(new EventHandler(this));
 buttondiv.addActionListener(new EventHandler(this)); 
 buttonopen.addActionListener(new EventHandler(this));
 buttonclose.addActionListener(new EventHandler(this)); 

 txtresult1.addKeyListener( new KeyHandler(this));

     
 }
 
 
/* This is class Attribute */ 
 public JButton button1,button2,button3,button4,button5,button6,button7,button8,button9,button0,buttonopen,buttonclose;
public JButton buttonplus,buttonminus,buttondiv,buttonmul,buttonans,buttonstart,buttonarrow,buttoncut;
 //EventHandler mouse=newEventHandler1();
public static int err=0,ans=0; 
JTextField txtresult1;
JFrame frame;
ArrayList arr;
public int c=0;
GuiCalculator gui;
KeyHandler key;
}
